# ポテパンキャンプ課題

#### 【課題１】環境のセットアップ

* Dockerインストール、起動
* プロジェクトのフォーク、クローン
* Docker-composeでの動作確認
* RuboCopとRSpecを導入
* herokuインストール、設定
* CircleCIでRuboCop,RSpecを通し、herokuへ自動デプロイ

#### 【課題２】商品詳細ページ実装

* （potepan/sample/single_product.html.erb）を参考にして、商品詳細ページを実装してください。
* 商品のモデル名は Spree::Product です。
* 実装するパス は /potepan/products としてください。
* ルーティングの定義には適切に namespace を利用してください。
* app/views/layouts/application.html.erb を使用して、ヘッダーの共通化を行ってください。
* https://lit-meadow-67238.herokuapp.com/potepan/products/1

#### 【課題３】カテゴリーページ実装

* （potepan/sample/product_grid_left_sidebar.html.erb）に選択したカテゴリーの商品が一覧で表示されるようにしてください。
* カテゴリー引数は taxonomies の id を取るように実装してください
* 実際のパスは /potepan/categories/:taxon_id/ となります。
* https://lit-meadow-67238.herokuapp.com/potepan/categories/1

### 使用技術

* Ruby2.5.1
* Ruby on Rails 5.2
* MySQL 8.0
* Docker/Docker-compose
* RSpec
* RuboCop/RuboCop Airbnb
* CircleCI
* AWS(S3)
* ...

### テスト

* RSpec
  * 単体テスト(model)
  * 機能テスト(request)
  * 統合テスト(feature)
