require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    it "タイトル表示されること" do
      expect(full_title("")).to eq "BIGBAG Store"
      expect(full_title(nil)).to eq "BIGBAG Store"
      expect(full_title("test")).to eq "test - BIGBAG Store"
    end
  end
end
