require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe '関連商品は同じカテゴリに紐付き重複しない商品のみ表示すること' do
    let(:taxon) { create(:taxon) }
    let(:other_taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:other_product) { create(:product, taxons: [other_taxon]) }

    it '重複商品、別カテゴリ商品は表示されないこと' do
      expect(product.related_products).not_to match_array([product, other_product])
    end
  end
end
