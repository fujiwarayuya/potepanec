require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "showページが正しく表示" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "正常なレスポンスを返すこと" do
      expect(response).to have_http_status "200"
      expect(response).to have_http_status(:success)
    end

    it "taxonomyを表示すること" do
      expect(response.body).to include taxonomy.name
    end

    it "taxonを表示すること" do
      expect(response.body).to include taxon.name
    end

    it "商品数を表示すること" do
      expect(response.body).to include taxon.products.count.to_s
    end

    it "productを表示すること" do
      expect(response.body).to include product.name
    end

    it "商品価格を表示すること" do
      expect(response.body).to include product.price.to_s
    end
  end
end
