require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "showページが正しく表示" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get potepan_product_path product.id
    end

    it "正常なレスポンスを返すこと" do
      expect(response).to have_http_status "200"
      expect(response).to have_http_status(:success)
    end

    it "商品を表示すること" do
      expect(response.body).to include product.name
      expect(response.body).to include product.description
    end

    it "関連商品を表示すること" do
      related_products.each do |related_product|
        expect(response.body).to include related_product.name
        expect(response.body).to include related_product.display_price.to_s
      end
    end
  end
end
