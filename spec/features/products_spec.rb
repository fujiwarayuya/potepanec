require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  given(:product) { create(:product, price: "10.00", taxons: [taxon]) }
  given(:other_taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  given!(:other_product) { create(:product, price: "20.00", taxons: [other_taxon]) }
  given!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "商品詳細ページを表示すること" do
    expect(page).to have_title product.name
    within '.pageHeader' do
      expect(page).to have_selector "h2", text: product.name
      expect(page).to have_selector "li", text: product.name
      expect(page).to have_link 'HOME'
    end
    within '.singleProduct' do
      expect(page).to have_link '一覧ページへ戻る'
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  scenario "関連商品を表示すること" do
    within '.productsContent' do
      related_products.each do |related_product|
        expect(page).to have_selector "h5", text: related_product.name
        expect(page).to have_selector "h3", text: related_product.display_price
        expect(page).to have_selector ".productBox", count: 4
      end
    end
  end

  scenario "同じ商品は関連商品に表示されないこと" do
    within '.productsContent' do
      expect(page).not_to have_selector "h5", text: product.name
      expect(page).not_to have_selector "h3", text: product.display_price
    end
  end

  scenario "別商品は関連商品に表示されないこと" do
    within '.productsContent' do
      expect(page).not_to have_selector "h5", text: other_product.name
      expect(page).not_to have_selector "h3", text: other_product.display_price
    end
  end

  scenario "一覧ページへリンクすること" do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
end
