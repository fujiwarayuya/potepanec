require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy, name: "Categories") }
  given(:taxon) { create(:taxon, name: "Bags", parent: taxonomy.root, taxonomy: taxonomy) }
  given!(:other_taxon) { create(:taxon, name: "Mags", parent: taxonomy.root, taxonomy: taxonomy) }
  given!(:product) { create(:product, name: "bag", taxons: [taxon]) }
  given!(:other_product) { create(:product, name: "mag", taxons: [other_taxon]) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "商品カテゴリページを表示すること" do
    visit potepan_category_path(taxonomy.root.id)
    expect(page).to have_title taxonomy.root.name
    expect(page).to have_selector 'h2', text: taxonomy.name
    expect(page).to have_selector 'li', text: taxonomy.name
    expect(page).to have_selector 'h5', text: product.name
    expect(page).to have_selector 'h3', text: product.display_price
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(page).to have_content "(#{taxon.products.count})"
      expect(page).to have_content "(1)"
      expect(page).to have_content other_taxon.name
      expect(page).to have_content other_taxon.products.count
      expect(page).not_to have_content other_product.name
    end
  end

  scenario 'サイドバーが挙動すること' do
    within '.side-nav' do
      click_on taxonomy.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  scenario "表示商品から商品詳細ページにアクセスすること" do
    expect(page).to have_link product.name
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario '他カテゴリ商品を表示すること' do
    visit potepan_category_path(other_taxon.id)
    expect(page).to have_content other_taxon.name
    expect(page).to have_content other_product.name
    expect(page).not_to have_content product.name
  end
end
